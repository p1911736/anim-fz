/*
 * CalculsRigidBody.cpp :
 * Copyright (C) 2016 Florence Zara, LIRIS
 *               florence.zara@liris.univ-lyon1.fr
 *               http://liris.cnrs.fr/florence.zara/
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/** \file CalculsMSS.cpp
 Programme calculant pour chaque particule i d un MSS son etat au pas de temps suivant
 (methode d 'Euler semi-implicite) : principales fonctions de calculs.
 \brief Fonctions de calculs de la methode semi-implicite sur un systeme masses-ressorts.
 */

#include <stdio.h>
#include <math.h>
#include <vector>
#include <iostream>

#include "vec.h"
#include "ObjetSimule.h"
#include "ObjetSimuleRigidBody.h"
#include "Viewer.h"

using namespace std;

/*
 * Calcul de la masse de l objet rigide
 */
void ObjetSimuleRigidBody::CalculMasse()
{
    for (unsigned int i = 0; i < _Nb_Sommets; i++)
    {
        _Mass += M[i];
    }

    // Calculer le barycentre
    _BaryCentre = Vector(0, 0, 0);

    for (unsigned int i = 0; i < _Nb_Sommets; i++)
    {
        _BaryCentre = _BaryCentre + M[i] * P[i];
    }

    _BaryCentre = _BaryCentre / _Mass;
}

/*
 * Calcul du tenseur d inertie de l objet rigide - - partie constante Ibody
 * et remplissage tableau roi (position particules dans repere objet)
 */
void ObjetSimuleRigidBody::CalculIBody()
{
    _ROi.resize(_Nb_Sommets);

    for (unsigned int i = 0; i < _Nb_Sommets; i++)
    {
        // Calculer la position de la particule i dans l'objet
        _ROi[i] = P[i] - _Position;
    }

    Matrix inertiaTensor;

    for (unsigned int i = 0; i < _Nb_Sommets; i++)
    {
        // Calculer les éléments du tenseur d'inertie pour chaque particule
        float xi = _ROi[i].x; // Coordonnée x de la particule i dans l'objet
        float yi = _ROi[i].y; // Coordonnée y de la particule i dans l'objet
        float zi = _ROi[i].z; // Coordonnée z de la particule i dans l'objet

        // Calcul des termes du tenseur d'inertie
        inertiaTensor.m_Values[0] = xi * xi;
        inertiaTensor.m_Values[1] = xi * yi;
        inertiaTensor.m_Values[2] = xi * zi;
        inertiaTensor.m_Values[3] = yi * xi;
        inertiaTensor.m_Values[4] = yi * yi;
        inertiaTensor.m_Values[5] = yi * zi;
        inertiaTensor.m_Values[6] = zi * xi;
        inertiaTensor.m_Values[7] = zi * yi;
        inertiaTensor.m_Values[8] = zi * zi;

        _Ibody += ((xi * xi + yi * yi + zi * zi) * Matrix::UnitMatrix() - inertiaTensor);
        _IbodyInv = _Ibody.InverseConst();
    }
}

/*
 * Calcul de l etat de l objet rigide.
 */
void ObjetSimuleRigidBody::CalculStateX()
{
    Matrix rotation;
    // Calculer la matrice de rotation
    for (unsigned int i = 0; i < 8; i++)
    {
        rotation.m_Values[i] = _Rotation.m_Values[i];
    }
    _Position = _Position - _BaryCentre;
    _InertieTenseurInv = rotation * _IbodyInv * rotation.TransposeConst();
    _VitesseAngulaire = _InertieTenseurInv * _MomentCinetique;
}

/*
 * Calcul de la derivee de l etat : d/dt X(t).
 */
void ObjetSimuleRigidBody::CalculDeriveeStateX(Vector gravite)
{
    _RotationDerivee = StarMatrix(_VitesseAngulaire) * _Rotation;
    _Force = _Mass * gravite;
    _Vitesse = _QuantiteMouvement / _Mass;

    _Torque = Vector(0, 0, 0);

    for (unsigned int i = 0; i < _Nb_Sommets; i++)
    {
        // Calculer le moment de force appliqué à la particule i
        _Torque = _Torque + cross(_ROi[i] - P[i], _Force);
    }
}

/**
 * Schema integration pour obbtenir X(t+dt) en fct de X(t) et d/dt X(t)
 */
void ObjetSimuleRigidBody::Solve(float visco)
{
    float d = _delta_t;
    _Position = _Position + _Vitesse * d;
    _QuantiteMouvement = _QuantiteMouvement + _Force * d;
    _Rotation = _Rotation + _RotationDerivee * d;
    _MomentCinetique = _MomentCinetique + _Torque * d;
    _VitesseAngulaire = _InertieTenseurInv * _MomentCinetique;

    for (unsigned int i = 0; i < _Nb_Sommets; i++)
    {
        P[i] = P[i] + _Vitesse * d;
    }
}

/**
 * Gestion des collisions avec le sol.
 */
void ObjetSimuleRigidBody::Collision()
{
    for (unsigned int i = 0; i < _Nb_Sommets; i++)
    {
        if (P[i].y <= -3)
        {
            // Si la particule i est en dessous du sol, on la replace sur le sol
            P[i].y = -3;
            _Vitesse.y = 0;
            _QuantiteMouvement.y = 0;
        }
    }
}
