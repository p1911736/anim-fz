/*
 * CalculsMSS.cpp :
 * Copyright (C) 2016 Florence Zara, LIRIS
 *               florence.zara@liris.univ-lyon1.fr
 *               http://liris.cnrs.fr/florence.zara/
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/** \file CalculsMSS.cpp
Programme calculant pour chaque particule i d un MSS son etat au pas de temps suivant
 (methode d 'Euler semi-implicite) : principales fonctions de calculs.
\brief Fonctions de calculs de la methode semi-implicite sur un systeme masses-ressorts.
*/

#include <stdio.h>
#include <math.h>
#include <vector>
#include <iostream>

#include "vec.h"
#include "ObjetSimule.h"
#include "ObjetSimuleMSS.h"
#include "Viewer.h"

using namespace std;

/**
 * Calcul des forces appliquees sur les particules du systeme masses-ressorts.
 */
void ObjetSimuleMSS::CalculForceSpring()
{
    /// f = somme_i (ki * (l(i,j)-l_0(i,j)) * uij ) + (nuij * (vi - vj) * uij) + (m*g) + force_ext

    /// Rq : Les forces dues a la gravite et au vent sont ajoutees lors du calcul de l acceleration

    Vector vent(5.0, 0.0, 0.0); // Vent horizontal

    for (unsigned int i = 0; i < _Nb_Sommets; i++)
    {
        Force[i].x = 0;
        Force[i].y = 0;
        Force[i].z = 0;

        Force[i] = Force[i] + vent;
    }

    for (unsigned int i = 0; i < _SystemeMasseRessort->_RessortList.size(); i++)
    {
        Ressort *ressort = _SystemeMasseRessort->_RessortList[i];

        Particule *pA = ressort->GetParticuleA();
        Particule *pB = ressort->GetParticuleB();

        if (ressort->GetRaideur() != 0.0 && ressort->GetAmortissement() != 0.0)
        {
            float lressort = length(P[pA->GetId()] - P[pB->GetId()]);
            if (lressort > 2.0)
            {
                ressort->SetRaideur(0.0);
                ressort->SetAmortiss(0.0);
            }

            Vector vec_distance = P[pA->GetId()] - P[pB->GetId()];

            Vector fRaideur = ressort->GetRaideur() * (length(vec_distance) - ressort->GetLrepos()) * normalize(vec_distance);
            Vector fAmortis = ressort->GetFactAmorti() * cross((V[pA->GetId()] - V[pB->GetId()]), normalize(vec_distance));

            Force[pA->GetId()] = Force[pA->GetId()] - fRaideur - fAmortis;
            Force[pB->GetId()] = Force[pB->GetId()] + fRaideur + fAmortis;
        }
    }
} // void

/**
 * Gestion des collisions avec le sol et une sphère.
 */
void ObjetSimuleMSS::Collision()
{
    /// Arret de la vitesse quand touche le plan

    for (unsigned int i = 0; i < _Nb_Sommets; i++)
    {
        if (P[i].y < -10)
        {
            V[i].y = 0;
        }
    }
} // void
