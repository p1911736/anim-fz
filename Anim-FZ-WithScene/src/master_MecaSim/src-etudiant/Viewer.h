/** \file Viewer.h
 * \brief Viewer de l application.
 */

#ifndef VIEWER_H
#define VIEWER_H

#include "glcore.h"

// Fichiers de gkit2light
#include "window.h"
#include "program.h"
#include "buffer.h"
#include "texture.h"
#include "mesh.h"
#include "draw.h"
#include "vec.h"
#include "mat.h"
#include "orbiter.h"
#include "app.h"

// Fichiers de master_meca_sim
#include "Scene.h"
#include "MSS.h"
#include "ObjetSimule.h"
#include "ObjetSimuleParticule.h"
#include "ObjetSimuleMSS.h"

using namespace std;

const int DIMW = 6;

class Viewer : public App
{
public:
    Viewer();
    Viewer(string *Fichier_Param, int NbObj);
    int init();
    int render();
    int quit();
    void help();
    int update(const float time, const float delta);

protected:
    Orbiter m_camera;
    DrawParam gl;

    Scene *_Simu;

    Vector MousePos;
    Vector MousePos2;

    int Tps;

    bool mb_cullface;
    bool mb_wireframe;

    bool b_draw_grid;
    bool b_draw_axe;

    Mesh m_axe;
    Mesh m_grid;
    Mesh m_cube;
    Mesh m_plan;
    Mesh m_sphere;
    Mesh m_terrain;
    Mesh m_cubeMap;
    Mesh m_carreArbre;
    Mesh m_house;

    GLuint m_cube_texture;
    GLuint m_tissu_texture;
    GLuint m_terrain_texture;
    GLuint m_cubeMap_texture;
    GLuint m_carreArbre_texture;
    GLuint m_house_texture;

    void init_axe();
    void init_grid();
    void init_cube();
    void init_sphere();
    void init_terrain(Mesh &m_terrain, const Image &im);
    void init_cubeMap();
    void init_carreArbre();
    void init_house();

    void draw_carreArbre(float t_x, float t_y, float t_z, float s_x, float s_y, float s_z);

    void init_plan(float x, float y, float z);
    void manageCameraLight();
};

#endif
