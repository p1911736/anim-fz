var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "_abcdfgimnoprstvwz",
  2: "abcdimnopstuvw",
  3: "abcdefghiklmnopqrstuvw~",
  4: "_abcdefhijklmnopqrstuvwxyz",
  5: "lp",
  6: "mo",
  7: "gm",
  8: "mu",
  9: "st"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "related",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Friends",
  7: "Macros",
  8: "Modules",
  9: "Pages"
};

